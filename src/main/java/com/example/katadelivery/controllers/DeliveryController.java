package com.example.katadelivery.controllers;

import com.example.katadelivery.dto.ClientDto;
import com.example.katadelivery.dto.DeliveryDto;
import com.example.katadelivery.service.DeliveryService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/delivery")
@RequiredArgsConstructor

public class DeliveryController {

    private final DeliveryService deliveryService;

    @PostMapping("/add")
    public ResponseEntity<String> addDelivery(@Valid @RequestBody DeliveryDto delivery) {
        deliveryService.addDelivery(delivery);
        return ResponseEntity.ok("delivery est ajouté");
    }
    @GetMapping
    public ResponseEntity<List<DeliveryDto>> getAllDelivery() {
        return ResponseEntity.ok(deliveryService.getAllDelivery());
    }
}
