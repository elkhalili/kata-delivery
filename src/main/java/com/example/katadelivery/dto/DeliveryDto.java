package com.example.katadelivery.dto;

import com.example.katadelivery.models.Client;
import com.example.katadelivery.models.DeliveryMode;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DeliveryDto {
    private DeliveryMode mode;
    @NotNull
    private String day;
    private Client client;
}
