package com.example.katadelivery.models;

public enum DeliveryMode {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
