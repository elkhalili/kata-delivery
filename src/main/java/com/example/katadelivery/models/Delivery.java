package com.example.katadelivery.models;

import jakarta.persistence.*;
import lombok.Data;
import java.util.UUID;

@Entity
@Data
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Enumerated(EnumType.STRING)
    private DeliveryMode mode;
    private String day;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
}
