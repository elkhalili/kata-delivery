package com.example.katadelivery.service;

import com.example.katadelivery.dto.ClientDto;
import com.example.katadelivery.dto.DeliveryDto;
import com.example.katadelivery.models.Delivery;

import java.util.List;

public interface DeliveryService {

   void  addDelivery(DeliveryDto Delivery);
   List<DeliveryDto> getAllDelivery();
}
