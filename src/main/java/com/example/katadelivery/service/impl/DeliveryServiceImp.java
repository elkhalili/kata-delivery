package com.example.katadelivery.service.impl;

import com.example.katadelivery.dto.ClientDto;
import com.example.katadelivery.dto.DeliveryDto;
import com.example.katadelivery.exceptions.ClientNotFoundException;
import com.example.katadelivery.models.Delivery;
import com.example.katadelivery.repository.ClientRepository;
import com.example.katadelivery.repository.DeliveryRepository;
import com.example.katadelivery.service.DeliveryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class DeliveryServiceImp implements DeliveryService {
    private final DeliveryRepository deliveryRepository;
    private final ClientRepository clientRepository;
    private final ObjectMapper mapper;

    @Override
    public void addDelivery(DeliveryDto delivery) {

        Delivery toSave = dtoToEntity(delivery);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        toSave.setClient(clientRepository.findByEmail(authentication.getName()).orElseThrow(ClientNotFoundException::new));
        deliveryRepository.save(toSave);
    }

    @Override
    public List<DeliveryDto> getAllDelivery() {
        return deliveryRepository.findAll().stream().map(this::entityToDto).toList();
    }

    private Delivery dtoToEntity(DeliveryDto delivery) {
        return mapper.convertValue(delivery, Delivery.class);
    }

    private DeliveryDto entityToDto(Delivery entity) {
        return mapper.convertValue(entity, DeliveryDto.class);
    }
}



